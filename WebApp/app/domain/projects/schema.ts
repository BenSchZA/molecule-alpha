import { schema } from 'normalizr';

const project = new schema.Entity('projects');
const projects = [project];

export default projects;