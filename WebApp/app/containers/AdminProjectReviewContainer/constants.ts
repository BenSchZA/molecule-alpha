/*
 *
 * AdminProjectReviewContainer constants
 *
 */

enum ActionTypes {
    APPROVE_PROJECT = "app/AdminProjectReviewContainer/APPROVE_PROJECT",
    REJECT_PROJECT = "app/AdminProjectReviewContainer/REJECT_PROJECT"
}

export default ActionTypes;
