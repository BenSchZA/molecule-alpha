/*
 *
 * MyProjectsContainer constants
 *
 */

/*
 *
 * MyProjectsContainer constants
 *
 */
enum ActionTypes {
  ADD_PROJECT = 'app/projects/ADD_PROJECT',
  GET_ALL_PROJECTS = 'app/projects/GET_ALL_PROJECTS',
  GET_PROJECTS = 'app/projects/GET_PROJECTS',
  GET_MY_PROJECTS = 'app/projects/GET_MY_PROJECTS',
  LAUNCH_PROJECT_REQUEST = 'app/MyProjects/LAUNCH_PROJECT_REQUEST',
  LAUNCH_PROJECT_SUCCESS = 'app/MyProjects/LAUNCH_PROJECT_SUCCESS',
  LAUNCH_PROJECT_FAILURE = 'app/MyProjects/LAUNCH_PROJECT_FAILURE',
  SUPPORT_PROJECT_REQUEST = 'app/MyProjects/SUPPORT_PROJECT_REQUEST',
  SUPPORT_PROJECT_SUCCESS = 'app/MyProjects/SUPPORT_PROJECT_SUCCESS',
  SUPPORT_PROJECT_FAILURE = 'app/MyProjects/SUPPORT_PROJECT_FAILURE',
  SET_MARKET_DATA = 'app/MyProjects/SET_MARKET_DATA',
}

export default ActionTypes;
