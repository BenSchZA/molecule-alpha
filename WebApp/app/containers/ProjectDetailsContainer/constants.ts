/*
 *
 * ProjectDetailsContainer constants
 *
 */

enum ActionTypes {
  DEFAULT_ACTION = 'app/ProjectDetailsContainer/DEFAULT_ACTION',
}

export default ActionTypes;
